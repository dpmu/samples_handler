/*
 * Samples_handler.h
 *
 *  Created on: 21 de abr de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_SAMPLES_HANDLER_H_
#define INCLUDE_SAMPLES_HANDLER_H_

/****************************************************************
 * Includes
 ****************************************************************/
#include "common_defines.h"
#include "read_stimulus_file.h"
#include "get_date_time.h" 									// Read serial port and take date time data.
#include "parameters_decoder_S-H.h"
#include <fcntl.h>
#include <sys/shm.h> //for memory share between the two process
#include <string.h> //used for memset usage --for fifo to c37118 program
#include <syslog.h> //sys log file rotation

//Baudrate constant
#define C_GPS_BAUDRATE B9600//230400 //115200//B230400  //B460800, B500000

//Amount of seconds that should be ignored from GPS when sync is lost
#define C_SECOND_AMOUNT_TO_SKIP_WHEN_LOSS_GPS_SYNC 30

//SERIAL flag and pin / register
#define C_GPS_SERIAL_PORT_NAME "/dev/ttyS4" //on Beaglebone, i use uart4 to read data. The uart 4 is mapped to dev/tty04

//Path to FIFO - HANDLER2PMU
#define C_FIFO_PATH "/tmp/handler2pmu/"
#define C_FIFO_PATHNAME "/tmp/handler2pmu/h2pfifo"  /* Path used on ftok for shmget key  */

//Path to FIFO - PRULOADR2PMU
#define C_FIFO_PRU_PATH "/tmp/PRU2samplerFIFO/"
#define C_FIFO_PRU_PATHNAME "/tmp/PRU2samplerFIFO/PRU_loadfifo" /* Path used on ftok for shmget key  */

#endif /* INCLUDE_SAMPLES_HANDLER_H_ */
