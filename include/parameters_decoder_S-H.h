/*
 * parameters_decoder_S-H.h
 *
 *  Created on: 05 of April of 2020
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_PARAMETERS_DECODER_S_H_H_
#define INCLUDE_PARAMETERS_DECODER_S_H_H_
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//Public defines
#define TRUE 1
#define FALSE 0

//Structures for decoded data
struct dpmu_file_decoded_st
{
	char *mag_adjust_constant[3]; //3 phases
	uint8_t stimulus_mode; //0 to use PRUSS samples source, 1 to stimulus file source
	uint8_t nominal_sys_freq;
};

//prototype of public functions
void decode_DPMU_file_parameters_pmu(void);
void return_dpmu_magnitude_adjusts(float *magnitudes_adjust);
uint8_t return_dpmu_stimulus_mode();
uint8_t return_dpmu_nominal_freq();

#endif /* INCLUDE_PARAMETERS_DECODER_S_H_H_ */
