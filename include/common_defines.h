/*
 * common_defines.h
 *
 *  Brief: This is a common defines file for Samples Handler.
 *  Created on: 5 de abr de 2020
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_COMMON_DEFINES_H_
#define INCLUDE_COMMON_DEFINES_H_

#include <stdint.h>


//BOOL variable definition
#define bool uint8_t
#define true 1
#define false 0

#define C_SAMPLES_PER_CYCLE 64 //Samples points per cycle.

#define C_WORK_INDEPENDENT_MODE 0 //Samples Handler works without oppenning the fifo between PMU and Samples Handler

////PMU file serial stimulus //TO BE REMOVED
#define C_SER_PMU_STIM_FILE0 "/tmp/serial_pmu_stimulus.txt"
#define C_FILE_HEADER_INITIAL_LINES 1 // Lines to be excluded on the first access //From TOP exported file
#define C_VALUE_SCALE 0 // Read values scale -> 0 =V/A; 3=KV/KA; 6=MV/MA
#define SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE (5*4) //5 float values * 4 byte

//ADC DATA (used with PRUSS employ) debug setings **Edit on pmu_beaglebone too
#define C_BUFFER_CIRCULAR 1
#define C_DEBUG_ADC_DATA_FILENAME0 "/tmp/ADC_read_data0.txt"
#define C_DEBUG_ADC_DATA_FILENAME1 "/tmp/ADC_read_data1.txt"
//#define C_DEBUG_ADC_DATA_FILENAME2 "/tmp/ADC_read_data2.txt"
#define C_CIRCULAR_ADC_DATA_FILENAMES_LENGTH 1536000 //100 seconds on 1920 samples per second


//Shared memory struct - Use on GPS //Removido do samples handler
struct shared_use_st {
    int ready_to_read;
    int can_obtain_new_soc; //Flag to allow an GPS uart open and get new soc frame.
    uint32_t SOC;
    bool wrong_data;
};

//Samples Handler specific parameters:
struct samples_values{
	//meter sources values
	float Va_mod;//[C_SAMPLES_PER_CYCLE];
	float Vb_mod;//[C_SAMPLES_PER_CYCLE];
	float Vc_mod;//[C_SAMPLES_PER_CYCLE];
	float Time;//[C_SAMPLES_PER_CYCLE]; // SOC; This is used from file input. counter is not used in this scheme
	uint16_t Counter; //This is used from rpmsg PRU comm. Time is not used in this scheme
	unsigned long int file_pointer; //File pointer returned AFTER the read access
	bool eof_file_flag;
};

#endif /* INCLUDE_COMMON_DEFINES_H_ */
