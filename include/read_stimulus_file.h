/*
 * read_stimulus_file.h
 *
 *  Created on: 21 de abr de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_READ_STIMULUS_FILE_H_
#define INCLUDE_READ_STIMULUS_FILE_H_



struct samples_values read_stimulus_file( int header_lines, long int file_l_pointer, uint8_t reserved);   // function definition
//int read_stimulus_file( int header_lines, long int file_l_pointer);   // function definition


#endif /* INCLUDE_READ_STIMULUS_FILE_H_ */
