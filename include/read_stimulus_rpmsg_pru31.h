/*
 * read_stimulus_rpmsg_pru31.h
 *
 *  Created on: 31 de mai de 2018
 *      Author: Maique Garcia
 */

#ifndef INCLUDE_READ_STIMULUS_RPMSG_PRU31_H_
#define INCLUDE_READ_STIMULUS_RPMSG_PRU31_H_

struct samples_values read_stimulus_rpmsg_pru31( uint8_t kickPRU1);   // function definition


#endif /* INCLUDE_READ_STIMULUS_RPMSG_PRU31_H_ */



