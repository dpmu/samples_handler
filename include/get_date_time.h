/*
 * get_date_time.h


//function to get date and time from GPS
//Author : Maique C Garcia
//Description:
//			This function computes the sequence components values
//--------------------------------------------------------------------------------
//Requirements:
//
//--------------------------------------------------------------------------------
//Version control
// Vr number     /   Date   / Description
// 0.1           / 11-07-17 / Created initial version
// 0.2           / 11-07-17 / Copy from Netbeans project
// 0.3			 / 14-07-19 /
 */

#ifndef INCLUDE_GET_DATE_TIME_H_
#define INCLUDE_GET_DATE_TIME_H_
#include <fcntl.h>
#include <stdint.h>
#include <time.h>
#include <termios.h>  //used to decode and access the serial port on BBB

#define C_GPS_FRAME_LENGTH 62 //respectiing the GPS NMEA frame            "$GPRMC,"  //Removido do Samples Handler

//GPS HEADER protocol
#define C_GPS_CHARACTER$ 36 //$
#define C_GPS_CHARACTERG 71 //G
#define C_GPS_CHARACTERP 80 //P
#define C_GPS_CHARACTERA 65 //A
#define C_GPS_CHARACTER_COMMA 44 //,
#define C_GPS_CHARACTER_ASTERISTIC 42 //*
#define C_GPS_CHARACTERZ 90 //Z
#define C_GPS_CHARACTERD 68 //D
#define C_GPS_CHARACTERR 82 //R
#define C_GPS_CHARACTERM 77 //M
#define C_GPS_CHARACTERN 78 //N
#define C_GPS_CHARACTERC 67 //C
#define C_GPS_CHARACTER_ENTER 10


struct return_get_date_data
{
	struct tm date_time;
    char gprmc_total_frame[73];
};
struct return_get_date_data get_date_time(char *portname, int gps_fd);   // function definition

#endif /* INCLUDE_GET_DATE_TIME_H_ */
