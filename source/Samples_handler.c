
/*
 * Samples_handler.c
 *
 * Version: 1.1
 * Date:	20/04/18
 * Author: Maique Correa Garcia
 *
 * In Eclipse add Include path
 *     C:\gcc-linaro\arm-linux-gnueabihf\libc\usr\include
 *
 *ABOUT PROGRAM:
 *		This program reads the data (FROM TOP saved stimulus from comtrade PMUs files) from file and drive it to the PMU OOOOOORRRRRRRR
 *		take the PRU adc samples (controlled by parameter C_D_PMU_OPPERATION_MODE ) also taking the SOC from serial pin (TO BE IMPLEMENTED)

Version Control
// Vr number     /   Date   / Description
//0.1			/ 20-04-18  / Initial version on Beaglebone architecture. COPY FROM VERSION 1.95 of Widora serial_handler
//1.0			/ 24-04-18  / Finished the first operational version but only for read stimulus from file until now. ToDo:read from Beaglebone PRU
//1.1			/ 25-04-18  / Starting the alteration to communicate with the PRU loader application.
//1.2			/ 30-05-18  / Starting the alteration to read the data from rpmsg_31 (with backup on 09-06-18)
//1.3		  	/ 09-06-18  / Changed to UIO prototype and trying the FIFO implementation here.
//1.4			/ 13-06-18  / Starting the PMU program alteration to comunicate the cycle per cycle allowing the Phasor calculus
//1.5			/ 14-01-18  / Changed to efficiency upgraded version
//1.6			/ 27-04-19  / Adicionado m�todo para detec��o de EOF (Modo offline) na pmu_beaglebone. Enviado -1 para todos os 4 floats.
//1.7			/ 14-07-19	/ Migrando para leitura do SOC aqui junto ao sampleshandler.
//1.8			/ 29-03-20	/ Cleaning code and removing unnecessary mode
//1.9			/ 22-09-20	/ Adding logging capability to register wrong samples counter increments
//1.93			/ 01/11/20  / added time.h library and SOC obtention refactor
//1.94			/ 08/11/20  / Added a ignore amount of GPS SOC to be skipped when a loss of sync occurs (GPS generate a not expected SOC).
//1.95			/ 14/11/20	/ Adding a secondary attempt to get a date based on the end '*' character. Solve wrong data when a intermediary comma character was lost by serial read.
//1.96			/ 15/11/20	/ Re-inserting the stimulus file support usage.
//2.0			/ 27/11/20	/ Add 50hz system nominal frequency support
//2.1			/ 03-07-21	/ Added support for $GNRMC GPS frame type.
//2.1.1			/ 13-09-21  / Refactored the uart manipulation and a new flush approach.
*/

//Code reference source: http://jkuhlm.bplaced.net/hellobone/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <signal.h> //used to fork and kill process
#include <errno.h> //http://www.virtsync.com/c-error-codes-include-errno CODES!!  //to debug values
#include "Samples_handler.h"
#include <syslog.h> //sys log file rotation
#include <time.h>

//See values on
//cat /var/log/syslog | grep Samples_handler
//write on file in this form '   syslog(LOG_ERR, "MAIQUE TESTE Syslog", valuesss )  ';


//////////////////////////////////////////////////////////////////////////////////////////////////////////
// 									Locally functions prototype											//
//																										//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
float CALC_AD_VALUE(int16_t AD_read_val);

int main(void) {
	puts("Serial Handler - starting - v2.1");
	syslog(LOG_INFO, "Running the PMU Serial handler program version %f", 2.1);
	char        *buffer;
	uint		i,j;
	struct samples_values meter;
    uint32_t temporary_soc_from_shared_mem, soc_time_calculated;
    time_t soc_time_calculated_GPS_process;
    //struct tm time_structure;
    struct return_get_date_data get_gps_structure;
    uint8_t _stimulus_mode; //refers to stimulus obtention mode (PRUSS if 0, stimulus file if 1).
    float _samples_per_second = 3840.0; //Default sampling frequency - for 60Hz system

	///Magnitude calibration factor calculus, based on iunput value
    float _magnitudes_calibrations[3];
    uint8_t _gps_soc_ignored_amount = 0; //amount of ignored SOC after a loss of sync //Can be removed when the RTC will be synced with NMEA and 1PPS
    //	GPIO configuration for debug behavior
    FILE *io,*iodir,*ioval;
    //P8_25 = GPIO 32 -- needs a cape overlay
    // P8_56 = GPIO 61

    io = fopen("/sys/class/gpio/export", "w");
    fseek(io,0,SEEK_SET);
    fprintf(io,"%d",61);
    fflush(io);

    iodir = fopen("/sys/class/gpio/gpio61/direction", "w");
    fseek(iodir,0,SEEK_SET);
    fprintf(iodir,"out");
    fflush(iodir);

    ioval = fopen("/sys/class/gpio/gpio61/value", "w");
    fseek(ioval,0,SEEK_SET);

    fprintf(ioval,"%d",0);
    fflush(ioval);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 							Open and mount C37118 parameters structure									//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	decode_DPMU_file_parameters_pmu();
	return_dpmu_magnitude_adjusts(_magnitudes_calibrations);
	_stimulus_mode = return_dpmu_stimulus_mode();

	if (return_dpmu_nominal_freq() == 60)
	{
		_samples_per_second = 3840.0;
	}
	else
	{
		_samples_per_second = 3200.0;
	}

	printf("Nominal decoded frequency: %d", return_dpmu_nominal_freq());
	/*Debug*/
	//printf("Phasors Magnitude adjust: A: %f  B: %f  C: %f \r\n", _magnitudes_calibrations[0], _magnitudes_calibrations[1], _magnitudes_calibrations[2]); //printf("IDCODE da D-PMU: %c\r\n", interpreted_data.dpmu_idcode[0]);
	//exit(EXIT_SUCCESS);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 									Shared memory and IPC configuration									//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	struct tm date_time;
	int shmid;
	void *shared_memory = (void *)0;
	struct shared_use_st *shared_stuff;
	srand((unsigned int)getpid());
	shmid = shmget((key_t)12345678, sizeof(shared_stuff), 0777 | IPC_CREAT);
	//shmid = shmget((key_t)1234, 43, 0777 | IPC_CREAT);
	if (shmid == -1) {
		fprintf(stderr, "shmid - memoria compartilhada para o fork failed PMU program\n");
		fprintf(stderr, "errno value: %d \n", errno);
		exit(EXIT_FAILURE);
	}
	shared_memory = shmat(shmid, (void *)0, 0);
	if (shared_memory == (void *)-1) {
		fprintf(stderr, "shmat memoria compartilhada para o fork failed PMU program\n");
		fprintf(stderr, "errno value: %d \n", errno);
		exit(EXIT_FAILURE);
	}
	shared_stuff = (struct shared_use_st *)shared_memory;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 										GPS get data process 											//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	shared_stuff->ready_to_read = 0;  //just an reset to Soc valid data flag
	if(_stimulus_mode == 0){ //take data from PRUSS - UIO

		pid_t pid_debug_write,pid=fork(); //separating process
		if (pid == 0) //Child process verification
		{
			//oppening the Serial port
			int gps_fd;
			uint8_t byte_read[1];

			struct termios options;

			if ((gps_fd = open(C_GPS_SERIAL_PORT_NAME, O_RDWR | O_NOCTTY)) < 0) //O_NDELAY
			{
				perror("Serial handler UART4: Failed to open the port.\n");
				syslog(LOG_ERR, "UART4: Failed to open the serial port with rturn value: %d", gps_fd );
				exit(EXIT_FAILURE);
			}
			tcgetattr(gps_fd, &options);
			options.c_cflag = C_GPS_BAUDRATE | CS8 | CREAD | CLOCAL;// B57600 | CS8 | CREAD | CLOCAL;
			options.c_lflag |= (ICANON| ECHO | ECHOE);	//Enable canonical mode.
			options.c_iflag = IGNPAR | ICRNL;
			options.c_oflag = 0; // Output flags - Turn off output processing
			tcflow(gps_fd, TCOON | TCION); //Enable hardware control
			tcsetattr(gps_fd, TCSANOW, &options);
			//close(gps_fd);



			shared_stuff->can_obtain_new_soc = 0;


			while(1)
			{
				//////////////////////////////////////////////////////////////////////////////////////////////////////////
				// 									Read and decode data from uart										//
				//																										//
				//////////////////////////////////////////////////////////////////////////////////////////////////////////

				//tcflush(gps_fd, TCIOFLUSH);
				//fflush(gps_fd);

				get_gps_structure = get_date_time(C_GPS_SERIAL_PORT_NAME, gps_fd);


				shared_stuff->wrong_data = false;

				if((get_gps_structure.date_time.tm_year < 2020)||(get_gps_structure.date_time.tm_year >= 2050)) //Its gonna work until 2050
				{
					//Debug
					printf("1 print: Complete GPRMC frame: ");
					for (i=0;i<=69; i++)
					{
						printf("%C",get_gps_structure.gprmc_total_frame[i]);
					}
					printf("\n");

					printf("2 print: Wrong year read. Data read is: %d/%d/%d %d:%d:%d \n", get_gps_structure.date_time.tm_mday, get_gps_structure.date_time.tm_mon, get_gps_structure.date_time.tm_year, get_gps_structure.date_time.tm_hour, get_gps_structure.date_time.tm_min, get_gps_structure.date_time.tm_sec);
					syslog(LOG_ERR, "GPS: Unwanted data received. look this date: %d/%d/%d %d:%d:%d \n", get_gps_structure.date_time.tm_mday, get_gps_structure.date_time.tm_mon, get_gps_structure.date_time.tm_year, get_gps_structure.date_time.tm_hour, get_gps_structure.date_time.tm_min, get_gps_structure.date_time.tm_sec);

					shared_stuff->wrong_data = true;
				}
				else
				{
					//////////////////////////////////////////////////////////////////////////////////////////////////////////
					// 										Estimate and obtain SOC											//
					//																										//
					//////////////////////////////////////////////////////////////////////////////////////////////////////////

					printf("Date read is: %d/%d/%d %d:%d:%d \n", get_gps_structure.date_time.tm_mday, get_gps_structure.date_time.tm_mon, get_gps_structure.date_time.tm_year, get_gps_structure.date_time.tm_hour, get_gps_structure.date_time.tm_min, get_gps_structure.date_time.tm_sec);

					get_gps_structure.date_time.tm_year -= 1900; //time.h expect years since 1900
					get_gps_structure.date_time.tm_mon -= 1; // time.h expect months since January � [0, 11] and NMEA returns read [1-12]
					get_gps_structure.date_time.tm_isdst = 0; // Is DST on? 1 = yes, 0 = no, -1 = unknown
					soc_time_calculated_GPS_process = mktime(&get_gps_structure.date_time); //Convert tm structure to SOC value

					//Debug
					//printf("1 print: Complete GPRMC frame: ");
					//for (i=0;i<=73; i++)
					//{
					//	printf("%C",get_gps_structure.gprmc_total_frame[i]);
					//}
					//printf("\n");


					if (get_gps_structure.gprmc_total_frame[2] == C_GPS_CHARACTERN) //Compensation for GPS IC type
					{
						shared_stuff->SOC = soc_time_calculated_GPS_process + 2;
						//puts("adding 2");
					}
					else
					{
						shared_stuff->SOC = soc_time_calculated_GPS_process + 1;
						//puts("adding 1");
					}


				}
				//Ready to read and handle SOC shared data, set flag
				//syslog(LOG_DEBUG,"SOC decoded and ready to read: %d", shared_stuff->SOC);
				shared_stuff->ready_to_read = 1;

	//			printf("Complete good GPRMC frame: ");
	//			for (i=0;i<=68; i++)
	//			{
	//				printf("%C",get_gps_structure.gprmc_total_frame[i]);
	//			}
	//			printf("\n");

				while(shared_stuff->can_obtain_new_soc == 0) // Wait a flag from the main processor to allow a new SOC obtention
				{
					//Manually clean UART during a not allowed SOC acquisition
					read(gps_fd, byte_read,1);
					//printf("discarding byte"); //Debug print
				}
			} //while 1
		}
}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 									Configure and open fifo to PMU 										//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
    int res_send;
	int pipe_fd;
	int open_mode =  O_WRONLY | O_NONBLOCK;//O_RDONLY; //O_NOMBLOCK serve para n�o bloquear a escrita na fifo e permite debugar quando ela esta cheia por conta da variavel de retorno (=-1 -> fifo full; = 255 -> fifo ok)
	float buffer_pipe[SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE];
	puts("oppening fifo PMU interface");
	memset(buffer_pipe, '\0', SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE);
#if (C_WORK_INDEPENDENT_MODE == 0)
	pipe_fd = open(C_FIFO_PATHNAME, open_mode);
	if (pipe_fd == -1)
	{
		printf("failed on open FIFO Handler 2 PMU program \r\n");
		syslog(LOG_ERR, "FIFO to pmu beaglebone: Failed to open fifo with the return: %d", pipe_fd );
		exit(EXIT_FAILURE);
	}
#endif
#if (C_WORK_INDEPENDENT_MODE & BIT0)
	puts("Independent mode ON - dont send data to PMU program");
	syslog(LOG_NOTICE, "Working with Independent mode ON. C_WORK_INDEPENDENT_MODE value: %d", C_WORK_INDEPENDENT_MODE );

#endif

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 								Configure and create fifo from PRU or file								//
	//																										//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
    int res_received;
	int pru_pipe_fd;
	int open_mode_PRU = O_RDONLY;//O_WRONLY;  //only read because the Samples Handler is the only one that can write
	uint16_t PRUbuffer_pipe[4];
	uint header_file_input_offset_bytes = C_FILE_HEADER_INITIAL_LINES;
	long int filereader_l_pointer = 0;

	if(_stimulus_mode == 0){ //take data from PRUSS - UIO
		puts("Stimulus from PRU - DRIVE UIO and FIFO");
		puts("creating fifo PRU interface - to PRU");
		syslog(LOG_NOTICE, "Taking data from PRUSS. Operate mode value: %d", _stimulus_mode);

		if (mkdir(C_FIFO_PRU_PATH,777) == -1)
		{
			remove(C_FIFO_PRU_PATHNAME);
			remove(C_FIFO_PRU_PATH);
		}
		mkdir(C_FIFO_PRU_PATH,777); //always re/create the file
		if (access(C_FIFO_PRU_PATHNAME, F_OK) == -1)
		{
			res_received = mkfifo(C_FIFO_PRU_PATHNAME, 0777);
			if (res_received != 0)
			{
				fprintf(stderr, "PMU program :Could not create fifo PRU2HANDLER %s\n", C_FIFO_PRU_PATHNAME);
				exit(EXIT_FAILURE);
			}
		}
		puts("Waiting PRU FIFO");
		pru_pipe_fd = open(C_FIFO_PRU_PATHNAME, open_mode_PRU);
		if (pru_pipe_fd == -1)
		{
			printf("PMU program :failed on open FIFO PRU2HANDLER");
			syslog(LOG_ERR, "Serial handler to PRUSS FIFO: Failed to create FIFO for PRUSS with return value: %d", res_received );

			exit(EXIT_SUCCESS);
		}
	}
	else //Take data from stimulus file
	{
		puts("File stimulus MODE ON");
		syslog(LOG_NOTICE, "Taking data from stimulus file. Operate mode value: %d", _stimulus_mode );
		meter.eof_file_flag = 1;
	}


    puts("Starting Samples handler");

    while(1) //LOOK_HR_MODE == true){ //first need locate the header bytes -START
    {

    	//////////////////////////////////////////////////////////////////////////////////////////////////////////
    	// 									Code logic to get samples from file									//
    	//																										//
    	//////////////////////////////////////////////////////////////////////////////////////////////////////////
    	if (_stimulus_mode==1) //(from file)
    	{
			//Reading samples
			meter = read_stimulus_file(header_file_input_offset_bytes, filereader_l_pointer, 0); //0 for reserved field
			header_file_input_offset_bytes = 0; //To be used only once
			filereader_l_pointer = meter.file_pointer;
			if(meter.eof_file_flag == 1)
			{
				puts("Finished Samples_handler - EOF arrived!");
				// Case occurs an EOF, the sampled data are changed to -1.0
				buffer_pipe[0] = -1.0;
				buffer_pipe[1] = -1.0;
				buffer_pipe[2] = -1.0;
				buffer_pipe[3] = -1.0;
				res_send = write(pipe_fd, buffer_pipe, SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE);

				exit(EXIT_SUCCESS); //ending program
			}

			buffer_pipe[0] = meter.Time;
			buffer_pipe[1] = meter.Va_mod;
			buffer_pipe[2] = meter.Vb_mod;
			buffer_pipe[3] = meter.Vc_mod;
			buffer_pipe[4] = meter.eof_file_flag;

			#if  (C_WORK_INDEPENDENT_MODE == 0)
				res_send = write(pipe_fd, buffer_pipe, SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE);
				if (res_send == -1) //try write until fifo is not full error handler
				{
					 //printf("sent the value  %02X %02X // %02X %02X // %02X %02X //120e121 = %02X %02X \n //", buffer_pipe[0],buffer_pipe[1],buffer_pipe[8], buffer_pipe[9], buffer_pipe[16], buffer_pipe[17], buffer_pipe[120], buffer_pipe[121]);
					 while(res_send < 0)
					 {
						 res_send = write(pipe_fd, buffer_pipe, SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE);
						 puts("Serial Handler: FIFO Meter2PMU ocupada - Tentando escrever novamente");
					 }
				 }
			#endif
		}
    	else //Read samples from PRUSS
		{

			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			//							Code logic to get samples PRU using Driver UIO								//
			//																										//
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Reading data PRU pipe PIPE
			res_received = read(pru_pipe_fd, PRUbuffer_pipe, 8); //each read have to read 2X the variables sent. reading 4 values of 16 bites resulting in only, 8 bytes.
			if (res_received == -1)
	        {
	            printf("Sample Handler program: PROBLEM DETECTED");
	        	syslog(LOG_CRIT, "Wrong read from PRUSS FIFO. Read returns: %d", res_received);

	        }
	        else if(res_received == 0)
	        {
	        	printf("PRU FIFO CLOSED, Closing APP.\n");
	        	exit(EXIT_SUCCESS);
	        }

	        //TIME soc error detection and update routine
	        if(PRUbuffer_pipe[0] == 1) //Check SOC just on the counter change
	        {
		        if( (shared_stuff->ready_to_read == 1) && (_gps_soc_ignored_amount == 0) ) //Get SOC from GPS only when there is data to read AND the D-PMU is ignoring the necessary amount of seconds
		        {
		        	if (shared_stuff->wrong_data == false) //Get data from shared mem.
		        	{
						temporary_soc_from_shared_mem = shared_stuff->SOC;
						shared_stuff->ready_to_read = 0;
		        	}
		        	else
		        	{
		        		_gps_soc_ignored_amount++; //Count ignored SOC
		        		temporary_soc_from_shared_mem = temporary_soc_from_shared_mem + 1;
			        	shared_stuff->ready_to_read = 0;
		        		printf("SOC time updated by software - GPS error. Last Soc val.: %d, SOC updated: %d, sample counter = %d \r\n", shared_stuff->SOC, temporary_soc_from_shared_mem, PRUbuffer_pipe[0]);
		        	}
	        		//printf("Get SOC (Value %d) at Counter = %d \r\n", shared_stuff->SOC, PRUbuffer_pipe[0]);

		        }
		        else
		        {
	        		//printf("Dont Get GPS generated SOC (Value on shared mem now: %d) at Counter = %d . incrementing artificially \r\n", shared_stuff->SOC, PRUbuffer_pipe[0]);
	        		_gps_soc_ignored_amount++; //Count ignored SOC
	        		temporary_soc_from_shared_mem = temporary_soc_from_shared_mem + 1;
		        	shared_stuff->ready_to_read = 0;
		        }

		        //Timeout to return getting timestamp from GPS
		        if (_gps_soc_ignored_amount >= C_SECOND_AMOUNT_TO_SKIP_WHEN_LOSS_GPS_SYNC)
		        {
		        	_gps_soc_ignored_amount = 0;
		        }

		        //Counter incrementing check
				if (temporary_soc_from_shared_mem != (soc_time_calculated+1))
				{
	        		printf("SOC incrementing error> Updated SOC %d; Last SOC: %d.  Shared mem SOC: %d\r\n", temporary_soc_from_shared_mem, soc_time_calculated, shared_stuff->SOC);
		        	syslog(LOG_ERR, "SOC incrementing error> Updated SOC %d; Last SOC: %d.  Shared mem SOC: %d\r\n", temporary_soc_from_shared_mem, soc_time_calculated, shared_stuff->SOC);
				}
				//Update SOC
				soc_time_calculated = temporary_soc_from_shared_mem;
        		shared_stuff->can_obtain_new_soc = 1;
	        }




	        //Counter incrementing check
	        if ((float)PRUbuffer_pipe[0] != (buffer_pipe[0] + 1.0))
	        {
	        	if ((buffer_pipe[0] != _samples_per_second) && ((float)PRUbuffer_pipe[0] != 1)) //Special case - second change
	        	{
		        	syslog(LOG_CRIT, "Wrong counts incrementation from PRUSS: SOC: %d", soc_time_calculated);
		        	syslog(LOG_CRIT, "Wrong counts incrementation from PRUSS: Last counter %f", buffer_pipe[0]);
		        	syslog(LOG_CRIT, "Wrong counts incrementation from PRUSS: Actual counter %f", (float)PRUbuffer_pipe[0]);
		        	printf("Wrong counter incrementation from PRUSS. Last coutner: %f   . Actual couter: %f", buffer_pipe[0], (float)PRUbuffer_pipe[0]);
	        	}
	        }

			buffer_pipe[0] = (float)PRUbuffer_pipe[0];//meter.Time[(i/5)];
			buffer_pipe[1] = _magnitudes_calibrations[0]*CALC_AD_VALUE((int16_t)PRUbuffer_pipe[1]); //meter.Va_mod[i];
			buffer_pipe[2] = _magnitudes_calibrations[1]*CALC_AD_VALUE((int16_t)PRUbuffer_pipe[2]); //meter.Vb_mod[i];
			buffer_pipe[3] = _magnitudes_calibrations[2]*CALC_AD_VALUE((int16_t)PRUbuffer_pipe[3]); //meter.Vc_mod[i];
			memcpy(&buffer_pipe[4], &soc_time_calculated, 4);							//Used Memcopy because the data truncation caused by the uint32_t -> float format conversion

			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			//									FIFO send samples and SOC data										//
			//																										//
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (C_WORK_INDEPENDENT_MODE == 0)
			{
				res_send = write(pipe_fd, buffer_pipe, SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE); //20 is compliant with 5 values in float variable
				if (res_send == -1) //try write until fifo is not full error handler
				{
					 //printf("sent the value  %02X %02X // %02X %02X // %02X %02X //120e121 = %02X %02X \n //", buffer_pipe[0],buffer_pipe[1],buffer_pipe[8], buffer_pipe[9], buffer_pipe[16], buffer_pipe[17], buffer_pipe[120], buffer_pipe[121]);
					 while(res_send < 0)
					 {
						 res_send = write(pipe_fd, buffer_pipe, SAMPLES_HANDLER_OUTPUT_BUFFER_SIZE);
						 printf("Samples Handle FIFO in send: look errno ");
						 printf("errno value: %d \n", errno);
					 }
				 }
			}
		}

    } //while(1)
}


/*Function  float CALC_AD_VALUE(int16_t AD_read_val)
 * Brief	This function returns the voltage level from the ADC binary word.
 * Param	AD_read_val - ADC read value (only in SIGNED 16 bits)
 * Return 	a float voltage sampled value
 */
float CALC_AD_VALUE(int16_t AD_read_val)
{
	float converted_val;
	int range = 10; //Range in volts  - From ADC configuration (deploy PRU0)
	float vref = 2.5;

	//converted_val = (float)((AD_read_val*4*2*range)/(131072*(vref/2.5))); //HEY: X4 BECAUSE THAT THE NUMBER IS IN 16 BYTES SIGNED,
	converted_val = (float)((AD_read_val*range<<3)/(131072*(vref/2.5))); //HEY: X4 BECAUSE THAT THE NUMBER IS IN 16 BYTES SIGNED,

	return converted_val;
}
