/*
 * parameters_decoder_S-H.h
 *
 *  Created on: 05 of April of 2020
 *      Author: Maique Garcia
 */

#include "../include/parameters_decoder_S-H.h"
#include <stdio.h>

#define C_PARAMETER_FILE "/usr/D-PMU_parameters/D-PMU_parameters.txt"

struct dpmu_file_decoded_st interpreted_data; //store data interpreted from the input file.

void decode_DPMU_file_parameters_pmu(void){

	FILE * fp;
	char * line = NULL;
	size_t len = 0;
	size_t length, length_header;

	//Open file
	fp = fopen(C_PARAMETER_FILE, "r");
	//check file health
	if (fp == NULL)
	{
		printf("Error. The Parameters file is empty.");
		exit(EXIT_FAILURE);
	}

	//structure reset.
	memset(&interpreted_data, 0x00, sizeof(interpreted_data));

	//Read and decode important lines (with '*' identifier )
	while ((length = getline(&line, &len, fp)) != -1) //seek file until it ends
	{

		if(line[0] == '*') //parameter identifier
		{
			if(strncmp(line,"*D-PMU_MAGNITUDE_ADJUST_PHASOR_A",32) == 0)
			{
				length = getline(&interpreted_data.mag_adjust_constant[0], &len, fp);
				printf("D-PMU magnitude adjust for Phasor A: %s\r\n", interpreted_data.mag_adjust_constant[0]);
			}
			else if(strncmp(line,"*D-PMU_MAGNITUDE_ADJUST_PHASOR_B",32) == 0)
			{
				length = getline(&interpreted_data.mag_adjust_constant[1], &len, fp);
				printf("D-PMU magnitude adjust for Phasor B: %s\r\n", interpreted_data.mag_adjust_constant[1]);
			}
			else if(strncmp(line,"*D-PMU_MAGNITUDE_ADJUST_PHASOR_C",32) == 0)
			{
				length = getline(&interpreted_data.mag_adjust_constant[2], &len, fp);
				printf("D-PMU magnitude adjust for Phasor C: %s\r\n", interpreted_data.mag_adjust_constant[2]);
			}
			else if(strncmp(line,"*STIMULUS_SOURCE",16) == 0)
			{
				length = getline(&line, &len, fp); //take response from stimulus source
				if(strncmp(line,"use_stimulus_file",17) == 0)
				{
					interpreted_data.stimulus_mode = 1;
				}
				printf("D-PMU stimulus source (0-PRUSS source, 1-Stimulus file): %d\r\n", interpreted_data.stimulus_mode);
			}
			else if (strncmp(line,"*NOMINAL_SYS_FREQUENCY",22) == 0)
			{
				length = getline(&line, &len, fp);
				interpreted_data.nominal_sys_freq = atoi(line);
				printf("Nominal configured frequency: %d\r\n", interpreted_data.nominal_sys_freq);
			}

			else
			{

			}
		}

	}
	puts("--------------------Decoding process finished--------------------------------");

	fclose(fp);
	//if (line)
	//	free(line);
	//exit(EXIT_SUCCESS);

};

/*Function  void return_dpmu_magnitude_adjusts(float *angles_adjust[3])
 * Brief	This function get the D-PMU magnitudes adjust for 3 phasors.
 * Param	pointer to float structure to be write.
 * Return 	none
 */
void return_dpmu_magnitude_adjusts(float *magnitudes_adjust)
{

	magnitudes_adjust[0] = (float)atof(interpreted_data.mag_adjust_constant[0]);
	magnitudes_adjust[1] = (float)atof(interpreted_data.mag_adjust_constant[1]);
	magnitudes_adjust[2] = (float)atof(interpreted_data.mag_adjust_constant[2]);
}

/*Function  void return_stimulus_source_mode(uint8_t stimulus_mode)
 * Brief	This function get the D-PMU magnitudes stimulus mode behavior
 * Param	pointer to byte flag to be write.
 * Return 	stimulus_mode
 */
uint8_t return_dpmu_stimulus_mode()
{

	return interpreted_data.stimulus_mode;
}



/*Function  void return_dpmu_nominal_freq()
 * Brief	This function get the configured grid frequency
 * Param	none.
 * Return 	grid_system_frequency
 */
uint8_t return_dpmu_nominal_freq()
{
	return interpreted_data.nominal_sys_freq;
}
