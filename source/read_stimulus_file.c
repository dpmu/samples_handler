/*
 * read_stimulus_file.c
 *
 *  Created on: 21 de abr de 2018
 *      Author: Maique Garcia
 */
#include "common_defines.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <errno.h>  //http://www.virtsync.com/c-error-codes-include-errno CODES!!

#include "read_stimulus_file.h"  /* Include the header (not strictly necessary here) */

struct samples_values read_stimulus_file( int header_lines, long int file_l_pointer, uint8_t reserved){

	FILE *fp;	//File reading var
	struct samples_values meter;
	char buf[255], teste;//File reading var
	register uint16_t i = 0; //Iterations to mount the read vector.

	meter.eof_file_flag = false;
	//Oppening file
	fp = fopen(C_SER_PMU_STIM_FILE0, "r"); //open in read mode
	//Reading values from the input file
	if (fp == NULL){
		puts("Error on opening FILE - ");
		fprintf(stderr, "errno value: %d \n", errno);
	}
	if (meter.eof_file_flag == false){ //Get only if the file isnt on the EOF
		//Moving to desired position
		fseek( fp, file_l_pointer, SEEK_SET );
		//Reading header unused lines
		if (header_lines > 0){
			while(fgetc(fp) != '\n'){ //puts("excluded 1 char");
			};
		};
		//Reading Cycle samples amount
		//while (i < C_SAMPLES_PER_CYCLE) {
			//Scanning the SOC
			fscanf(fp,"%f",&meter.Time); //This function scan until the new value (excluding the nulls, spaces and tab characters)
			//printf("Time get is : %f ",meter.Time[i]);
			//meter.Time = meter.Time*(pow(10,C_VALUE_SCALE)); //scalling factor
				//printf("Time get is : %f ",meter.Time[i]);
			//Scanning the VA
			fscanf(fp,"%f",&meter.Va_mod);
			meter.Va_mod = meter.Va_mod*(pow(10,C_VALUE_SCALE)); //scalling factor
				//printf("VA get is : %f ",meter.Va_mod[i]);
			//Scanning the VB
			fscanf(fp,"%f",&meter.Vb_mod);
			meter.Vb_mod = meter.Vb_mod*(pow(10,C_VALUE_SCALE)); //scalling factor
				//printf("VB get is : %f ",meter.Vb_mod[i]);
			//Scanning the VC
			fscanf(fp,"%f",&meter.Vc_mod);
			meter.Vc_mod =meter.Vc_mod*(pow(10,C_VALUE_SCALE)); //scalling factor
				//printf("VC get is : %f \n",meter.Vc_mod[i]);
			//i++;
		//} //i < C_SAMPLES_PER_CYCLE
	}; //(meter.eof_file_flag == true)

    if(!feof(fp) == 0){
    	puts("END OF FILE");
    	meter.eof_file_flag = true;
    };

	meter.file_pointer = ftell(fp); //reading the last file position
	fclose(fp);//Closing file
	return meter;

};
