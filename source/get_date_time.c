/*
 * get_date_time.c
 *
 *  Created on: 19 de jun de 2018
 *      Author: Maique Garcia
 */


//Sequency Components Calc script
//Author : Maique C Garcia
//Description:
//			This function computes the sequence components values
//--------------------------------------------------------------------------------
//Requirements:
// Phases phasors
//--------------------------------------------------------------------------------
//Version control
// Vr number     /   Date   / Description
// 0.1           / 11-07-17 / Created initial version
// 0.2			 / 27-07-18 / Implementing the Reason RT430 NMEA GPZDA frame support - Frame example: $GPZDA,191922.0,27,07,2018,,*5F
// 0.3			 / 21-01-19 / Added end character comparisson on $GPRMC (avoid wrong head detection)
// 0.4			 / 29-03-20 / Bugfix for frames where the total number of characters are different between 2 frames GPRMC,
//								decode based in comma separators. Removed unnintended support of GPZDA frame.
// 0.5			 / 03-07-21	/ Added support for TELIT SL869 V2 ICs - $GNRMC datetime header frames.


//Notes:
// Frames supported:
//$GPRMC,192130.000,A,2249.2888,S,04703.9684,W,0.1,0.0,270718,0.0,W*73     		--SL869
//$GNRMC,192130.000,A,2249.2888,S,04703.9684,W,0.1,0.0,270718,0.0,W*73     		--SL869V2

//Utility links:
//http://aprs.gids.nl/nmea/#gga - Reference of frames
#include <stdio.h>
#include <stdlib.h>
#include "get_date_time.h"
#include <errno.h> //http://www.virtsync.com/c-error-codes-include-errno CODES!!  //to debug values

struct return_get_date_data get_date_time(char *portname, int gps_fd)
{
    ssize_t length_data = 0;
    uint8_t byte_read[1], i;//, forcont;
    uint8_t flag = 0;
    uint8_t frame_count = 0;//,forcont=0;
    uint8_t complete_obtention = 0;
    uint8_t field_counter = 0;
    //struct tm date_time;
    char data_received[6]; //Date and time are composed by 6 characters
    char gprmc_total_frame[69];

    char gprc_entire_frame[80];
    uint8_t gprmc_index=0;
    struct return_get_date_data final_data;


    //Reset struct values
    final_data.date_time.tm_hour = 0;
    final_data.date_time.tm_min = 0;
    final_data.date_time.tm_sec = 0;
    final_data.date_time.tm_year = 0;
    final_data.date_time.tm_mon = 0;
    final_data.date_time.tm_mday = 0;

    while(complete_obtention == 0){
        byte_read[0] = 0;
        length_data = read(gps_fd, byte_read,1);


        // printf("caracter is %c,  dataread = %0X , errno = %d \n", byte_read[0] , byte_read[0], errno);
        if(length_data > 0) //received data - there is data to read on file
        {
            //Frame recognition
            if(flag == 1)
            {
                if(byte_read[0] == C_GPS_CHARACTERG) //G
                {
                	final_data.gprmc_total_frame[gprmc_index] = byte_read[0];
                	gprmc_index++;
                    flag = 2;
                    //puts("Found G");
                }
                else
                {
                    flag = 0;
                }
            }
            else if(flag == 2)
            {
                if((byte_read[0] == C_GPS_CHARACTERP) || (byte_read[0] == C_GPS_CHARACTERN))//P or N
                {
                	final_data.gprmc_total_frame[gprmc_index] = byte_read[0];
                	gprmc_index++;
                    flag = 3;
                    //puts("Found P or N");
                }
                else
                {
                    flag = 0;
                }  //P or N found
            }
            else if(flag == 3)
            {
                if(byte_read[0] == C_GPS_CHARACTERR)
                {
                	final_data.gprmc_total_frame[gprmc_index] = byte_read[0];
                	gprmc_index++;
                    flag = 4;
                    //	puts("Found R");
                }
                else
                {
                    flag = 0;
                } //R
            }
            else if(flag == 4)
            {
                if(byte_read[0] == C_GPS_CHARACTERM) //M
                {
                	final_data.gprmc_total_frame[gprmc_index] = byte_read[0];
                	gprmc_index++;
                    flag = 5;
                    //	 puts("Found M");
                }
                else
                {
                    flag = 0;
                } //M found
            }
            else if(flag == 5)
            {
                if(byte_read[0] == C_GPS_CHARACTERC) //C
                {
                	final_data.gprmc_total_frame[gprmc_index] = byte_read[0];
                	gprmc_index++;
                    flag = 6;
                    //puts("Found C");
                }
                else
                {
                    flag = 0;
                }
            }
            else if(flag == 6)
            {
                if(byte_read[0] == C_GPS_CHARACTER_COMMA) //Comma
                {
                	final_data.gprmc_total_frame[gprmc_index] = byte_read[0];
                	gprmc_index++;
                	flag = 0; //resets the behaviour
                	//Read all entire GPRMC frame:
            		length_data = read(gps_fd, gprc_entire_frame, 80); //always take a complete GPRMC frame
            		for (i=0; i <= 69; i++)
            		{
            			final_data.gprmc_total_frame[i+5] = gprc_entire_frame[i];
            		}
            		complete_obtention = 1;
                    //puts("Found ,");
                }
                else
                {
                    flag = 0;
                } //, found
            }
            else
            {
            	flag = 0;
                if(byte_read[0] == C_GPS_CHARACTER$) //$ found
                {
                    flag = 1;
                    gprmc_index =0;
                    final_data.gprmc_total_frame[gprmc_index] = byte_read[0];
                	gprmc_index++;
   //                 puts("Found $");
                }
            } // if flag ==  cases
        } //if length >0
    } //While1

    //Decoding the read frame
	final_data.date_time.tm_hour = ((gprc_entire_frame[0]-48)*10) + (gprc_entire_frame[1]-48);
	final_data.date_time.tm_min = ((gprc_entire_frame[2]-48)*10) + (gprc_entire_frame[3]-48);
	final_data.date_time.tm_sec = ((gprc_entire_frame[4]-48)*10) + (gprc_entire_frame[5]-48);


	i=0;
	gprmc_index=5;
	while (i <= 7) //Bypass comma separated fields
	{
		gprmc_index++;
		if (gprc_entire_frame[gprmc_index] == C_GPS_CHARACTER_COMMA)
		{
			i++;
		}
	}
	final_data.date_time.tm_mday = ((gprc_entire_frame[gprmc_index+1]-48)*10) + (gprc_entire_frame[gprmc_index+2]-48);
	final_data.date_time.tm_mon = ((gprc_entire_frame[gprmc_index+3]-48)*10) + (gprc_entire_frame[gprmc_index+4]-48);
	final_data.date_time.tm_year = ((gprc_entire_frame[gprmc_index+5]-48)*10) + (gprc_entire_frame[gprmc_index+6]-48) + 2000;

	//Read date with final "*" character as reference position - ERROR case.
	if ((final_data.date_time.tm_mon < 1) || (final_data.date_time.tm_mon > 12) || (final_data.date_time.tm_mday < 1) || (final_data.date_time.tm_mday > 31) || (final_data.date_time.tm_year < 2020) || (final_data.date_time.tm_year > 2050))
	{
		flag =	0;
		gprmc_index = 0;
		while( flag == 0) //If it reach position 10, there is no possibility to get the date
		{
			gprmc_index++;
			if (gprc_entire_frame[gprmc_index] == C_GPS_CHARACTER_ASTERISTIC)
			{
				flag = 1; //Set to look for a date,
			}
		}

		//Last attempt to get date from a final frame "111120,,,A*66"
		final_data.date_time.tm_mday = ((gprc_entire_frame[gprmc_index-10]-48)*10) + (gprc_entire_frame[gprmc_index-9]-48);
		final_data.date_time.tm_mon = ((gprc_entire_frame[gprmc_index-8]-48)*10) + (gprc_entire_frame[gprmc_index-7]-48);
		final_data.date_time.tm_year = ((gprc_entire_frame[gprmc_index-6]-48)*10) + (gprc_entire_frame[gprmc_index-5]-48) + 2000;
	}
	//TODO: RECHECK and get year by the frames end (fix datetime error when the missing character is a comma on the middle position in frame


    return final_data; //Finish this function


}
